﻿using UnityEditor;

public class BaseScriptTemplate
{
    [MenuItem("Assets/Create/Templates/MonoBehaviour", false, 1)]
    public static void CreateScript()
    {
        TemplateUtil.AddCSharpClassTemplate("Behaviour", "Untitled",
              "using UnityEngine;"
            + "\n"
            + "\npublic class " + TemplateUtil.CLASS_NAME + " : MonoBehaviour"
            + "\n{"
            + "\n}"
       );
    }
}
