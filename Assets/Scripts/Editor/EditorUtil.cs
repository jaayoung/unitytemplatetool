﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class EditorUtil
{
    public static string ConvertFullAbsolutePathToAssetPath(string fullPath)
    {
        fullPath = Path.GetFullPath(fullPath);

        var assetFolderFullPath = Path.GetFullPath(Application.dataPath);

        if (fullPath.Length == assetFolderFullPath.Length)
        {
            return "Assets";
        }

        var assetPath = fullPath.Remove(0, assetFolderFullPath.Length + 1).Replace("\\", "/");
        return "Assets/" + assetPath;
    }
}
