﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TemplateUtil : MonoBehaviour
{
    public const string CLASS_NAME = "CLASS_NAME";

    public static void AddCSharpClassTemplate(string typeName, string defaultFileName, string templateString)
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (Path.HasExtension(path))
        {
            path = Path.GetDirectoryName(path);
        }

        var absolutePath = EditorUtility.SaveFilePanel(
            "Choose name for " + typeName,
            path,
            defaultFileName + ".cs",
            "cs");

        if (absolutePath == "")
        {
            // Dialog was cancelled
            return;
        }

        if (!absolutePath.ToLower().EndsWith(".cs"))
        {
            absolutePath += ".cs";
        }

        var className = Path.GetFileNameWithoutExtension(absolutePath);
        File.WriteAllText(absolutePath, templateString.Replace(CLASS_NAME, className));

        AssetDatabase.Refresh();

        var assetPath = EditorUtil.ConvertFullAbsolutePathToAssetPath(absolutePath);

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
    }
}
